#include <stdio.h>
#include <stdlib.h>
#define MAX 1024*1024*50 // 50MB

typedef unsigned char bytes;

extern bytes key[16]; // 16字节密钥
extern bytes T[256];
extern bytes S[256];


void swap_bytes(bytes* a, bytes* b); // 交换字节
int myscanf(bytes* a); // 转换字符形式的十六进制输入 例如将"0x39c5bb"转换为0x39c5bb 返回输入字节数
void init(); // rc4初始化
void rc4(bytes* in, bytes* out, int length); // rc4加密函数
void myprintf(bytes* a, int count);
int readfile(char* fname, bytes* content); // 从文件读取字节 返回字节数
void writefile(char* fname, bytes* content, int size); // 将content的前size个字节写入文件fname

