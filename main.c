#include "rc4.h"

int main()
{
    char fname_in[256];
    char fname_out[256];
    int len;
    bytes* in = (bytes*) malloc(MAX * sizeof(bytes));
    bytes* out = (bytes*) malloc(MAX * sizeof(bytes));

    printf("密钥:");
    myscanf(key);
    printf("输入文件路径:");
    scanf("%s", fname_in);
    printf("输出文件路径:");
    scanf("%s", fname_out);
    len = readfile(fname_in, in);
    init();
    rc4(in, out, len);
    writefile(fname_out, out, len);

    return 0;
}
