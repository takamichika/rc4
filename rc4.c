#include "rc4.h"

bytes key[16];
bytes T[256];
bytes S[256];

void swap_bytes(bytes* a, bytes* b)
{
    bytes tmp = *a;
    *a = *b;
    *b = tmp;
    return;
}

int myscanf(bytes* a)
{
    char* buffer = (char*) malloc((MAX * 2 + 20) * sizeof(char));
    char* pp;
    bytes sum = 0;
    int count_bytes = 0;

    scanf("%s", buffer);
    pp = buffer;
    while (*pp != '\0')
    {
        if (*pp >= '0' && *pp <= '9') // 高位是数字
        {
            sum += (*pp - '0') * 16;
            if (*(pp + 1) >= '0' && *(pp + 1) <= '9') // 低位是数字
                sum += *(pp + 1) - '0';
            else if (*(pp + 1) >= 'a' && *(pp + 1) <= 'f') // 低位是a-f
                sum += *(pp + 1) - 'a' + 10;
        }

        else if (*pp >= 'a' && *pp <= 'f') // 高位是a-f
        {
            sum += (*pp - 'a' + 10) * 16;
            if ( *(pp + 1) >= '0' && *(pp + 1) <= '9' ) // 低位是数字
                sum += *(pp + 1) - '0';
            else if ( *(pp + 1) >= 'a' && *(pp + 1) <= 'f' ) // 低位是a-f
                sum += *(pp + 1) - 'a' + 10;
        }
        count_bytes++; // 计数器+1
        *a = sum; // sum写入a
        a++; // a偏移一位
        sum = 0; // 重置sum
        pp += 2; // 一字节两个十六进制字符 指针偏移两位
    }

    free(buffer);
    return count_bytes;
}

void init()
{
    int m, n = 0;

    for (int i = 0; i < 256; ++i)
    {
        T[i] = key[i % 16]; // 进行轮转，直到填满
        S[i] = i; // 按照升序，给每个字节赋值0,1,2,3,4,5,6.....,254,255
    }

    for (m = 0; m < 256; ++m)
    {
        n = (n + S[m] + T[m]) % 256;
        swap_bytes(&S[m], &S[n]);
    }

    return;
}


void rc4(bytes* in, bytes* out, int length)
{
    int m = 0, n = 0;
    bytes t;

    for (int i = 0; i < length; ++i)
    {
        m = (m + 1) % 256;
        n = (n + S[m]) % 256;
        swap_bytes(&S[m], &S[n]);
        t = (S[m] + S[n]) % 256;
        out[i] = in[i] ^ S[t];
    }

    return;
}

void myprintf(bytes* a, int count)
{
    for (int i = 0; i < count; ++i)
        printf("%x ", *(a + i));
    printf("\n");
}

int readfile(char* fname, bytes* content)
{
    FILE* fp;
    int n = 0; // 指针偏移量 同时也是读取的字节数
    fp = fopen(fname, "rb");
    while (!feof(fp))
    {
        fread(content + n, 1, 1, fp);
        n++;
    }
    n--;
    fclose(fp);
    return n;
}

void writefile(char* fname, bytes* content, int size)
{
    FILE* fp;
    fp = fopen(fname, "wb");
    fwrite(content, size, 1, fp);
    fclose(fp);
    return;
}


